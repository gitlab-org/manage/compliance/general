<Replace XX.X in New work links below with current Milestone>

<details>
<summary>Boards</summary>

- [Build Board](https://gitlab.com/groups/gitlab-org/-/boards/1305010)  
- [Next Up Board](https://gitlab.com/groups/gitlab-org/-/boards/4657720)  
- [UX Board](https://gitlab.com/groups/gitlab-org/-/boards/1576904)  
- Error budgets - [Grafana](https://dashboards.gitlab.net/d/stage-groups-compliance/stage-groups-group-dashboard-manage-compliance?orgId=1)/[Sisense](https://app.periscopedata.com/app/gitlab/891029/Error-Budgets-Overview)  

</details>

<details>
<summary>Team Capacity</summary>

Actual capacity: 00 W   
Stretch capacity (+20%): 00 W

~frontend - 00 W  
~backend - 00 W

~"type::feature" (60%): 00 W  
~"type::maintenance" (30%): 00 W  
~"type::bug" (10%): 00 W  

</details>

## Cross Functional Planning

### Planning ~"type::feature"

1. [Carry-over Board](https://gitlab.com/groups/gitlab-org/-/boards/1305010?label_name[]=type%3A%3Afeature)

#### Priority list 

1. Priority issue/epic
1. Priority issue/epic
1. Priority issue/epic


### Planning ~"type::maintenance"

1. [Carry-over Board](https://gitlab.com/groups/gitlab-org/-/boards/1305010?label_name[]=type%3A%3Amaintenance)
1. [New Board](https://gitlab.com/groups/gitlab-org/-/boards/4657720?not[label_name][]=type%3A%3Afeature&label_name[]=group%3A%3Acompliance&label_name[]=type%3A%3Amaintenance&not[milestone_title]=XX.X)

### Planning ~"type::bug"

1. [Carry-over Board](https://gitlab.com/groups/gitlab-org/-/boards/1305010?label_name[]=type%3A%3Abug)
1. [New Board](https://gitlab.com/groups/gitlab-org/-/boards/4657720?not[label_name][]=type%3A%3Afeature&label_name[]=group%3A%3Acompliance&label_name[]=type%3A%3Abug&not[milestone_title]=XX.X)

## Release Post Items

[All release posts](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&state=opened&label_name[]=group%3A%3Acompliance&label_name[]=release%20post)

1. Issue list

> _This issue and linked pages contain information related to upcoming products, features, and functionality.  It is important to note that the information presented is for informational purposes only. Please do not rely on this information for purchasing or planning purposes.  As with all projects, the items mentioned in this video and linked pages are subject to change or delay. The development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc._

/label  ~"group::compliance" ~"devops::govern" ~"section::sec" ~"Planning Issue" ~"type::ignore"
/assign @khornergit @nrosandich
/milestone %
/epic https://gitlab.com/groups/gitlab-org/software-supply-chain-security/compliance/-/epics/2
