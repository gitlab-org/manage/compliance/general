### Summary

_from https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/144#note_1540927827_

> With the first Compliance group hackathon being such a success and [feedback asking for it to happen more often](https://gitlab.com/gl-retrospectives/govern/compliance/-/issues/38#note_1523781366), we are proposing to use the final two days of every milestone as a mini hackathon to add audit events (18th-20th)
>
> This has two big impacts on the team, firstly producing audit events and value for users. The other is increasing the connectivity/learning of the team by :pear: and mob programming.
> 
> We have plenty of audit events that need adding in the following two epics:
>
> * https://gitlab.com/groups/gitlab-org/gl-security/-/epics/1+
> * https://gitlab.com/groups/gitlab-org/-/epics/736+
>
> We will continue to keep the [quarterly hackathons](https://gitlab.com/groups/gitlab-org/govern/compliance/-/epics/6) for other topics such as AI or other unknowns

### Dates

<Month> 18th - 20th

### Milestone

%"<Milestone>"

### Scheduled Zoom Drop-in Pairing Rooms

<Zoom Meeting>

- Timezone 1 (04:00 UTC - 12:00 UTC)
- Timezone 2 (12:00 UTC - 20:00 UTC)

Timezones were based on everyones location and overlap using https://www.timeanddate.com/worldclock/meeting.html
